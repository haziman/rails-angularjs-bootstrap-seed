(function(){
    'use strict'

    angular.module('myApp.modules', [
        'user_login',
        'sample',
    ])
    angular.module('myApp', [
        'ngSanitize', 'ngRoute', 'ngResource', 'ng-token-auth', 'templates', 'datatables', 'ui.bootstrap',
        'myApp.providers', 'myApp.controllers', 'myApp.filters', 'myApp.services', 'myApp.directives', 'myApp.modules'
    ])
        .config(config)
        .run(runBlock)

    /** @ngInject */
    function config($routeProvider, $authProvider) {
        $routeProvider.when('/home', {templateUrl: 'home.html', controller: "GenericViewCtrl"})
        $routeProvider.otherwise({redirectTo: '/home'});

        $authProvider.configure({
			apiUrl: '/api/v1',
            validateOnPageLoad: true
		});
    }

    /** @ngInject */
    function runBlock($rootScope, $auth, $location, bsNav, $interval, $route)
    {
        $rootScope.$on('$locationChangeStart', function(ev, next, current)
        {
            var path = $location.path()
            if( path == "" || path == "/" )
                return true
            if( path.indexOf('/login') != -1 )
                return true
            else if ( path.indexOf('/home') != -1 )
                return true

            if( !$auth.user.id && bsNav.restrictedPath(path) ){
                ev.preventDefault()
                
                var e = $interval(function(){
                    if($auth.user.id)
                    {
                        $interval.cancel(e)
                        $rootScope.$currentUser = $auth.user
                        $route.reload()
                    } else {
                        $interval.cancel(e)
                        localStorage.setItem('loginRedirect', next.split('#!')[1])
                        $location.path('/login')
                    }
                }, 200)
            }
        })

        function handleSessionError() {
            localStorage.setItem('loginRedirect', $location.path())
            $location.path('/login')
        }
        $rootScope.$on('auth:validation-error', handleSessionError)
        $rootScope.$on('auth:invalid', handleSessionError)
    }
})()