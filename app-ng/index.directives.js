(function(){ 
  'use strict'
  /* Directives */

  angular.module('myApp.directives')
    .directive('appVersion', ['version', function(version) {
      return function(scope, elm, attrs) {
        elm.text(version)
      }
    }])
    .directive('bootstrapNav', bootstrapNav)
  
  /** @ngInject */
  function bootstrapNav(bsNav, $location, $auth)
  {
    return {
      restrict: "E",
      templateUrl: 'bootstrap-nav.html',
      controller: function($scope)
      {
        $scope.secondaryMenus = bsNav.getSecondaryMenus()
        $scope.menus = bsNav.getMenus()
        $scope.isActive = function(href)
        {
          if(!href) return false
          if(href[0] == '#' && href[1] == '!')
            href = href.substring(2)
          return $location.path().indexOf(href) != -1
        }
        $scope.getLabel = function(menu)
        {
          if(typeof(menu.label) == "function"){
            return menu.label($auth, menu, $location)
          }
          return menu.label
        }
        $scope.restricted = function(code)
        {
          return bsNav.restricted(code)
        }
      },
      replace: true
    }
  }
})()
