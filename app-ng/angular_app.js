//= require angular-rails-templates

//= require index.module
//= require index.controllers
//= require index.directives
//= require index.filters
//= require index.services
//= require index.providers
//= require_tree ./views
//= require_tree ./modules
//= require index.init
//= require_self

angular.element(document).ready(function() {
    angular.bootstrap(document, ["myApp"]);
});