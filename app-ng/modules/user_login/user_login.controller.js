(function(){
    'use strict'

    angular.module('user_login')
        .controller("UserLoginCtrl", UserLoginCtrl)
    
    /** @ngInject */
    function UserLoginCtrl($scope, $auth, $location)
    {
        var vm = this

        vm.user = {}
        vm.errorMessages = []

        vm.attemptLogin = attemptLogin

        function attemptLogin()
        {
            $auth.submitLogin(vm.user)
                .then(function(resp) {
                    var loginRedirect = localStorage.getItem('loginRedirect')
                    console.log(loginRedirect)
                    $location.path(loginRedirect)
                }, function(resp) {})
        }

        $scope.$on('auth:login-error', function(ev, reason)
        {
            vm.errorMessages = reason.errors
        })
    }
})()