(function(){
    'use strict'

    angular.module('user_login', [])
        .config(config)

    /** @ngInject */
    function config(bsNavProvider, $routeProvider, msApiProvider)
    {
        bsNavProvider.addSecondaryMenu('user_login.index', {
            label: function($auth, $menu, $location){
                if(!$auth.user.id) {
                    $menu.href = "#!/login"  
                    return "Login"
                } else {
                    $menu.ngClick = function(){
                        $auth.signOut().then(function(){
                            $location.path('/login')
                        }, function(){
                            console.log('failed')
                        })
                    }
                    $menu.href = null
                    return "Logout"
                }
            }
        })

        $routeProvider.when('/login', {
            templateUrl: 'modules/user_login/index.html', 
            controller: 'UserLoginCtrl as vm',
            params: {
                next: null
            }
        })
    }
})()