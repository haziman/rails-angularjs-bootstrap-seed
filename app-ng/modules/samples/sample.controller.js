(function(){
    'use strict'

    angular.module('sample')
        .controller("SampleCtrl", SampleCtrl)
    
    /** @ngInject */
    function SampleCtrl($scope, DTOptionsBuilder, DTColumnBuilder, $uibModal, Roles)
    {
        var vm = this

        // DATATABLES

        vm.dtOptions = DTOptionsBuilder
            .fromSource('/books.json?' + (new Date).getTime())
            .withDataProp('data')
            .withPaginationType('full_numbers')

        vm.dtColumns = [
            DTColumnBuilder.newColumn('id').withTitle('ID').withOption('orderable', false),
            DTColumnBuilder.newColumn('title').withTitle('Book'),
            DTColumnBuilder.newColumn('author').withTitle('Author')
        ]

        // MODALS

        vm.openModal = function (parentSelector) {
            var $modalScope = $scope.$new()
            $modalScope.content = "This is modal content"
            $modalScope.title = "I am a modal!"

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'modal.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$ctrl',
                resolve: {},
                scope: $modalScope
            })

            modalInstance.result.then(function (selectedItem) {
                console.log('success')
            }, function () {
                console.log('fail')
            })
        }

        // console.log(Roles);
    }
})()