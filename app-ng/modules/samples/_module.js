(function(){
    'use strict'

    angular.module('sample', [])
        .config(config)

    /** @ngInject */
    function config(bsNavProvider, $routeProvider, msApiProvider)
    {
        bsNavProvider.addMenu('sample.index', { label: 'Samples', href: '#!/samples', restricted: true })
        msApiProvider.register('roles', ['/api/v1/roles/:id.json', { id: '@id' }])

        $routeProvider.when('/samples', {
            templateUrl: 'modules/samples/index.html', 
            controller: 'SampleCtrl as vm',
            resolve: {
                Roles: function( msApi ){
                    return msApi.resolve('roles@get')
                }
            }
        })

        
    }
})()