'use strict'

angular.module('myApp.directives', [])
angular.module('myApp.filters', [])
angular.module('myApp.services', [])
angular.module('myApp.controllers', [])
angular.module('myApp.providers', [])