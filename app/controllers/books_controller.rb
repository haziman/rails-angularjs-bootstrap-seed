class BooksController < ApplicationController
  before_action :override_datatable_params, only: [:index]

  def index
    respond_to do |f|
      f.json { render json: BookDatatable.new( params ) }
    end
  end

  private
  def override_datatable_params
    params["columns"] ||= { "0" => {"data" => "" } }
    params["length"]  ||= -1
  end
end
