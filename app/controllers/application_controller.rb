class ApplicationController < ActionController::Base
    def default_angular_view_handler
        render 'angular_app/main'
    end
end
