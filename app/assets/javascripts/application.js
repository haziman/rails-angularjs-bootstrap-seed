// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
/*
<script src="lib/jquery/jquery.min.js"></script>
<script src="lib/bootstrap/bootstrap.min.js"></script>
<script src="lib/angular/angular.min.js"></script>
*/

//= require jquery
//= require rails-ujs
//= require activestorage
//= require bootstrap/dist/js/bootstrap
//= require datatables.net/js/jquery.dataTables
//= require datatables.net-bs/js/dataTables.bootstrap
//= require angular/angular

//= require angular-route/angular-route
//= require angular-sanitize/angular-sanitize
//= require angular-resource/angular-resource
//= require angular-cookie/angular-cookie
//= require angular-datatables/dist/angular-datatables
//= require angular-ui-bootstrap/dist/ui-bootstrap-tpls
//= require ng-token-auth/dist/ng-token-auth
//= require angular_app