class BookDatatable < AjaxDatatablesRails::ActiveRecord

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      id: { source: "Book.id", cond: :eq },
      title: { source: "Book.title", cond: :like },
      author: { source: "Book.author", cond: :like }
    }
  end

  def data
    records.map do |record|
      {
        id: record.id,
        title: record.title,
        author: record.author
      }
    end
  end

  def get_raw_records
    Book.all
  end

end
