require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Backendtestv2
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.generators.test_framework :rspec

    config.angular_templates.module_name    = 'templates'
    config.angular_templates.ignore_prefix  = %w(views/)
    config.angular_templates.inside_paths   = ['app-ng']
    config.angular_templates.markups        = %w(erb str)
    config.angular_templates.extension      = 'html'
  end
end
