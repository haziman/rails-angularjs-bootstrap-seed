Rails.application.routes.draw do
  scope :api do 
    mount V1::Engine, at: '/v1'
  end
  
  get 'books' => 'books#index'

  root to: 'web#landing'
end
