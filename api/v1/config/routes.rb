V1::Engine.routes.draw do
    mount_devise_token_auth_for 'V1::User', at: 'auth', controllers: {
        sessions: 'v1/devise_token_auth/sessions'
    }

    resources :roles
end
