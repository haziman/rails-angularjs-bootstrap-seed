RspecApiDocumentation.configure do |config|
    config.format = :JSON
    config.docs_dir = V1::Engine.root.join("doc", "api")
end