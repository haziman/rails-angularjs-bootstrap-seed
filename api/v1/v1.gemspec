$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "v1/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "v1"
  s.version     = V1::VERSION
  s.authors     = ["HAZIMAN BIN HASHIM"]
  s.email       = ["haziman@abh.my"]
  s.homepage    = "http://github.com/jimanx2"
  s.summary     = "API V1"
  s.description = "API V1"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib,spec}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  s.test_files = Dir["spec/**/*_spec.rb"]

  s.add_dependency "rails", "~> 5.2.1"

  s.add_development_dependency "sqlite3"

  s.add_dependency "apitome"
  s.add_dependency "rspec_api_documentation"
  s.add_dependency "ajax-datatables-rails"
  s.add_dependency "database_cleaner"
  s.add_dependency "fast_jsonapi"
  s.add_dependency "devise"
  s.add_dependency "devise_token_auth"
  s.add_dependency "rspec-rails", "~> 3.8"
  
end
