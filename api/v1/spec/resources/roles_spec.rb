$LOAD_PATH << File.expand_path("../../", __FILE__)

require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Roles" do
  get ENGINE_ENDPOINT + "/v1/roles.json" do
    example "Listing roles" do
      do_request

      expect(status).to eq 200
    end
  end
end