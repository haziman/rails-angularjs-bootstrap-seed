$LOAD_PATH << File.expand_path("../../", __FILE__)

require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Bankhistories" do
  get ENGINE_ENDPOINT + "/v1/bankhistories.json" do
    example "Listing" do
      do_request
      expect(status).to eq 200
    end
  end
end