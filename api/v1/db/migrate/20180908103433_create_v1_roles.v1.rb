# This migration comes from v1 (originally 20180908103234)
class CreateV1Roles < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_roles do |t|
      t.string :code
      t.string :name

      t.timestamps
    end
  end
end
