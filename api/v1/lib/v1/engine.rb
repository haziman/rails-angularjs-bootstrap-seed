module V1
  class Engine < ::Rails::Engine
    isolate_namespace V1

    config.generators do |g|
      g.test_framework :rspec
      # g.fixture_replacement :factory_girl, :dir => 'spec/factories'
      g.assets false
      g.helper false
      g.template_engine false
    end
  end
end
