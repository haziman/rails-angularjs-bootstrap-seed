module V1
    class Railtie < Rails::Railtie
        initializer "rspec" do 
            load V1::Engine.root.join("config", "initializers", "rspec.rb")
        end

        initializer "apitome" do
            load V1::Engine.root.join("config", "initializers", "apitome.rb")
        end

        initializer "devise" do
            load V1::Engine.root.join("config", "initializers", "devise.rb")
        end

        initializer "devise_token_auth" do 
            load V1::Engine.root.join("config", "initializers", "devise_token_auth.rb")
        end

        initializer "append_paths" do |config|
            config.paths["db/migrate"] << V1::Engine.root.join("db", "migrate")
            config.paths["lib/tasks"] << V1::Engine.root.join("lib", "tasks")
        end
    end
end
