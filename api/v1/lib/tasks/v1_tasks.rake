require 'rspec/core/rake_task'

namespace :v1 do
  desc 'Generate API request documentation from API specs'
  RSpec::Core::RakeTask.new('docs:generate') do |t|
    t.pattern = V1::Engine.root.join("spec", "acceptance")
    t.rspec_opts = ["--format RspecApiDocumentation::ApiFormatter"]
  end
end