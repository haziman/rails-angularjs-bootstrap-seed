require "fast_jsonapi"
require "rspec_api_documentation"
require "database_cleaner"
require "apitome"
require "devise"
require "devise_token_auth"
require "v1/engine"
require "v1/railtie" #if defined?(Rails)

module V1
  # Your code goes here...
end
