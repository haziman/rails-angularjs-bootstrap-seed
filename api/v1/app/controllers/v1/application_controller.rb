module V1
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :null_session
    include ::DeviseTokenAuth::Concerns::SetUserByToken

    before_action :authenticate_v1_user! if Rails.env.production?

    private
      def default_render
        @subject = params[:controller].split('/').last
        @model = @subject.singularize
        @action = params[:action]

        serializer_name = "V1::#{@subject.singularize.titlecase}Serializer"
        serializer = nil
        begin
          serializer = serializer_name.constantize
        rescue Exception => ex
          Rails.logger.error ex.inspect
          serializer = DefaultSerializer
        end

        case @action
        when "index";
          @all = instance_variable_get("@#{@subject}")
          render json: serializer.new(@all).serialized_json
        when "show", "new", "update";
          @record = instance_variable_get("@#{@model}")
          render json: serializer.new(@record).serialized_json
        end
      end

  end
end