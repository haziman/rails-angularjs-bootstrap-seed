module V1
    module DeviseTokenAuth
        class SessionsController < ::DeviseTokenAuth::SessionsController
            protect_from_forgery with: :null_session
        end
    end
end