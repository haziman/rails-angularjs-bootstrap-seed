require "v1/default_serializer"

module V1
  class BankhistorySerializer < DefaultSerializer
    attributes :idx, :datetime, :detail, :debit, :credit
  end
end
