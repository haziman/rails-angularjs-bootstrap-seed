module V1
  class DefaultSerializer
    include FastJsonapi::ObjectSerializer
    attributes :id
  end
end
