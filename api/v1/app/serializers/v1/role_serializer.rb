require "v1/default_serializer"

module V1
  class RoleSerializer < DefaultSerializer
    attributes :name, :code
  end
end
